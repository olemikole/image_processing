# Image Processing
A program for processing images using OpenCv in C++. The images are downloaded to a folder (*original*), through a REST API call. Then the downloaded image will be processed by means of downscaling to a maximum width or height of 300 pixels. Two image processing techniques, image threshholding(`cv::threshold`) and adding text (`cv::putText`), will be applied respectively. Finally the image will be written to a output folder(*output*). Examples of the unprocessed and processed images are presented below.

### Example 1
<p float="left">
  <img src="original/cat5.jpg"/> 
  <img src="output/cat5.jpg" />
</p>

### Example 2
<p float="left">
  <img src="original/cat4.jpg"/> 
  <img src="output/cat4.jpg" />
</p>



## Usage
```bash
# Create and enter build directory
$ mkdir build
$ cd build

# Build project
$ cmake .. 
$ make

# Run program
$ ./image_processing
```

## Requirements
`cmake`, `make` and `g++`
### Required Libraries
[restclient-cpp](https://github.com/mrtazz/restclient-cpp)

[nlohmann/json](https://github.com/nlohmann/json)

[OpenCv](https://github.com/opencv/opencv)

## Authors
[Karl Munthe (@karl.munthe)](https://gitlab.com/karl.munthe)

[Kai Chen (@kjchen93)](https://gitlab.com/kjchen93)

[Ole Kjepso (@olemikole)](https://gitlab.com/olemikole)
