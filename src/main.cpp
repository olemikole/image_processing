#include <restclient-cpp/restclient.h>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <filesystem>

int main()
{

    std::string url;
    while(true)
    {
        //Parse json
        auto body = RestClient::get("https://aws.random.cat/meow").body;
        int url_start = body.find('"',body.find(':'))+1;
        int url_end = body.find('"',url_start);
        url = body.substr(url_start,url_end-url_start);
        url.erase( remove(url.begin(), url.end(),'\\'), url.end() );

        //The API sometimes returns gifs. We don't want that
        if(url.substr(url.size()-3) == "jpg")
            break;
        std::cout<<url<<" is a gif. Trying agian!\n\n";
    }
    
    //Find the next available filename
    int file_number = 1;
    std::string out_filename = "cat"+std::to_string(file_number)+".jpg";
    while(std::filesystem::exists("../output/"+out_filename))
    {
        file_number++;
        out_filename = "cat"+std::to_string(file_number)+".jpg";
    }

    //Download image
    std::string cmd = "wget -O ../original/"+out_filename+" "+url;
    system(cmd.c_str());

    //Open downloaded file
    cv::Mat image = cv::imread("../original/"+out_filename);

    //Resizing
    float scale = 300.0/MAX(image.cols, image.rows);  
    //Image size is only scaled if it is larger than 300px in one direction  
    scale = MIN(scale, 1);                                  
    cv::resize(image,image, cv::Size(),scale, scale,cv::INTER_LINEAR);

    //Make gray and apply binary threshold
    cv::cvtColor(image,image,cv::COLOR_BGR2GRAY);
    cv::threshold(image,image,120,255,0);
    cv::cvtColor(image,image,cv::COLOR_GRAY2BGR);
    
    
    
    //Put names
    cv::putText(image,
        "Kai Chen",
        cv::Point(10,image.rows*1/6),
        cv::FONT_HERSHEY_COMPLEX,
        image.cols/255.0,cv::Scalar(50,50,255),2
        );
    cv::putText(image,
        "Ole Kjepso",
        cv::Point(10,image.rows/2),
        cv::FONT_HERSHEY_COMPLEX,
        image.cols/255.0,cv::Scalar(50,255,50),2
        );
    
    cv::putText(image,
        "Karl Munthe",
        cv::Point(10,(image.rows*5)/6),
        cv::FONT_HERSHEY_COMPLEX,
        image.cols/225.0, cv::Scalar(255,50,50),2);


    cv::imwrite("../output/"+out_filename,image,{cv::IMWRITE_JPEG_QUALITY});
}